\documentclass{article}

\usepackage{amsmath}
\usepackage{mathpazo}
\usepackage[DIV=11]{typearea}
\usepackage{hyperref}
\usepackage{cleveref}

\title{Notes on inhomogenous elastic half-spaces}
\author{Lucas Frérot}
\date{\today}

\newcommand{\calC}{\mathcal{C}}
\newcommand{\calCH}{\calC^\mathrm{H}}
\newcommand{\calM}{\mathcal{M}}
\newcommand{\calN}{\mathcal{N}}
\newcommand{\calMH}{\calM^\mathrm{H}}
\newcommand{\calNH}{\calN^\mathrm{H}}
\newcommand{\eigen}{\epsilon^\mathrm{eq}}
\newcommand{\dC}{\Delta\calC}
\newcommand{\sgrad}{\nabla^\mathrm{sym}}
\renewcommand{\vec}[1]{{#1}}


\begin{document}
\maketitle

Consider a half-space $B$ with an arbitrary distribution of elastic properties
$\calC(x)$, $x\in B$. Consider a homoegenous elasticity tensor $\calCH$ constant
in $B$. We work with the assomption that we can define two operators for the
homogeneous half-space (with elastic properties $\calCH$), based on Green's
functions:
\begin{itemize}
\item $\calMH$ is the operator that computes equilibrium displacements from a
  distribution of surface tractions acting on $\partial B$;
\item $\calNH$ is the operator that computes equilibrium displacements from a
  distribution of eigenstresses acting in the volume of $B$.
\end{itemize}
The former is computed thanks to the Boussinesq--Cerruti Green's tensor (which
can be applied in the Fourier domain), the latter thanks to the Mindlin Green's
tensor (which can also be applied in the Fourier domain, see
\href{https://doi.org/10.1016/j.cma.2019.04.006}{\texttt{doi:10.1016/j.cma.2019.04.006}}
for details). Since both operators are linear, we can represent any equilibrium
displacements in the \textbf{homogeneous domain} due to surface tractions
$\vec{t}$ and volumetric eigenstresses $\vec{\sigma}$ as:
\begin{equation}\label{eqn:disp}
  \vec{u} = \calNH[\vec{\sigma}] + \calMH[\vec{t}].
\end{equation}

The principle of Eshelby's equivalent inclusion is to find a volumetric
distribution of \emph{eigenstrains} $\eigen$ such that one can solve equilibrium for the
inhomogenous case (with $\calC(x)$) by using the homogeneous operators $\calMH$
and $\calNH$. The starting point is to say that the stress state of the
homogeneous and inhomogeneous half-spaces should be identical, that is:
\begin{align}
  \calC(x):\epsilon & = \calCH:(\epsilon - \eigen),\\
  \Leftrightarrow \calCH:\eigen & = (\calCH - \calC(x)):\epsilon =
                                  \dC(x):\epsilon.\label{eqn:eigen}
\end{align}
Note that within the above expression, $\epsilon$ and $\eigen$ are both
unknowns. We have to add an equation to solve for either. Let's take the
symmetric gradient of \cref{eqn:disp}, with eigenstresses $\sigma = \calCH:\eigen$,
\begin{align}
  \sgrad u = \epsilon & = \sgrad\calNH[\calCH:\eigen] + \sgrad\calMH[t],\\
                      & = \sgrad\calNH[\dC(x):\epsilon] + \sgrad\calMH[t],\\
  \Leftrightarrow \epsilon - \sgrad\calNH[\dC(x):\epsilon] & = \sgrad\calMH[t],\\
  \Leftrightarrow \left(I - \sgrad\calNH \circ \dC(x)\right):\epsilon & = \sgrad\calMH[t],
\end{align}
which is a linear equation with unknown $\epsilon$ (the symbol $\circ$ denotes
function composition, which can be seen here as a matrix product since the
operators are linear). After having solved for $\epsilon$, we can write the
\textbf{inhomogeneous equilibrium displacements} as:
\[ u = \calNH[\dC(x):\epsilon] + \calMH[t]. \]

At the cost of a linear solve step (which is non-negligeable, especially since
$\eigen$ depends on $t$, and therefore needs to be recomputed if the surface
pressure changes), this approach allows great flexibility in the inhomogeneity
distribution $\calC(x)$, because the Green's functions needed are those of a
homogeneous half-space and are already known and efficiently computed in the
Fourier domain. For a half-space with a layer and substrate, the method is
certainly slower than the straight Green's function for the inhomogenous case,
but for more complex inhomogeneity cases, deriving Green's functions becomes impractical.

The method can even be extended to include a pressure dependency
of $\calC$, i.e.\ $\calC = \calC(x, \mathrm{tr}(\sigma)/3)$. In such a case, the
equation to solve with $\epsilon$ as unknown becomes implicit and non-linear.
This is costly to solve, but on the other hand I do not see an obvious way to
include pressure dependency with a straight Green's function method.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-master: t
%%% End:
