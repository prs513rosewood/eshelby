#!/usr/bin/env python3

import sys
import tamaas as tm
import numpy as np
import matplotlib.pyplot as plt

from copy import deepcopy
from scipy.sparse.linalg import LinearOperator, aslinearoperator, bicgstab
from tamaas.dumpers import NumpyDumper


def normal_layered_green(model, layer_model):
    """Plane-strain Green's function from Li & Popov (2020)"""
    shape = model.boundary_shape
    L = model.system_size[1:]
    h = layer_model.system_size[0]
    q = np.fft.fftfreq(shape[1], d=L[1]/shape[1]) * 2 * np.pi
    q = np.abs(q)

    E2, nu2 = model.E, model.nu
    E1, nu1 = layer_model.E, layer_model.nu

    A = (
        (E2 * (3 - 4 * nu1) * (1 + nu1) - E1 * (3 - 4 * nu2) * (1 + nu2))
        * (E1 * (1 + nu2) - E2 * (1 + nu1))
    )

    B = 4 * (
        (E2 * (1 + nu1) + E1 * (3 - 4 * nu2) * (1 + nu2))
        * (E1 * (1 + nu2) - E2 * (1 + nu1))
    )

    C = (
        E1**2 * (4 * nu2 - 3) * (1 + nu2)**2
        - 2 * E1 * E2 * (1 + nu1) * (2 * nu1 - 1) * (nu2 + 1) * (2 * nu2 - 1)
        + E2**2 * (8 * nu1**2 - 12 * nu1 + 5) * (1 + nu1)**2
    )

    D = (
        (E2 * (1 + nu1) + E1 * (3 - 4 * nu2) * (1 + nu2))
        * (E2 * (3 - 4 * nu1) * (1 + nu1) + E1 * (1 + nu2))
    )

    with np.errstate(divide='ignore'):
        G = 2 / layer_model.E_star * (
            (A * np.exp(-4 * q * h) + B * q * h * np.exp(- 2 * q * h) + D)
            / (q * (-A * np.exp(-4 * q * h) - B * q**2 * h**2 * np.exp(-2 * q * h)
                    + 2 * C * np.exp(-2 * q * h) + D))
        )

    G[0] = 0

    return G


def normal_layered_disp(model, layer_model, traction):
    pfft = np.fft.fft2(traction)
    G = normal_layered_green(model, layer_model)
    return np.fft.ifft2(G[np.newaxis, :] * pfft).real


# Problem variables
# The number of points in x direction is set to 2 because we look at
# a plane-strain problem to compare to the analytical solution,
# but the method is fully 3D
Nx = 2
Ny = 3**3
Nz = 256
L = 1
Lz = 1e-1  # thickness of layer
tau = 1
R = L / 24  # radius of shear circle
direction = 2  # 0 = x, 1 = y, 2 = z, direction of applied load

# Model for the homogeneous of the half-space
model = tm.Model(tm.model_type.volume_2d, [Lz, L, L], [Nz, Nx, Ny])

# Same Poisson ratio
model.nu = 0.0

# Set Young's modulus for layer
layer_model = deepcopy(model)
layer_model.E *= 0.5

# Register the volume integral operators (Mindlin)
tm.ModelFactory.registerVolumeOperators(model)
tm.ModelFactory.registerVolumeOperators(layer_model)

# Need to set integration method to cutoff:
# It is a bit slower but it does not care about
# the layer thickness
for op in ["mindlin", "mindlin_gradient"]:
    for m in [model, layer_model]:
        tm.ModelFactory.setIntegrationMethod(m.operators[op],
                                             tm.integration_method.cutoff,
                                             1e-10)

# Surface stress distribution
x = np.linspace(0, L, Nx, endpoint=False)
y = np.linspace(0, L, Ny, endpoint=False)
xx, yy = np.meshgrid(x, y, indexing='ij', sparse=True)

# Applied load in the specified direction
model.traction[..., direction] = tau * np.sin(2 * np.pi * yy / L)

# Construct the RHS of the linear equation we want to solve
model['rhs'] = np.zeros(model.shape + [6])  # 6 strain components in Voigt
model.operators['boussinesq_gradient'](model.traction, model['rhs'])

# Flatten
rhs = model['rhs'].reshape(-1)

# Define the linear operators in the equation to solve
C1 = aslinearoperator(layer_model.operators['hooke'])
C2 = aslinearoperator(model.operators['hooke'])
ΔC = C2 - C1
grad_N2 = aslinearoperator(model.operators['mindlin_gradient'])

# Define the linear operator we want to invert
A = LinearOperator(shape=[rhs.size, rhs.size],
                   matvec=lambda x: x - grad_N2 * ΔC * x)

# Solve for the total deformation
ɛ, _ = bicgstab(A, rhs)

# Equivalent eigenstresses
σ = ΔC * ɛ
σ = σ.reshape(model.shape + [6])

# Displacement due to surface tractions
substrate_surf_disp = np.zeros_like(model.displacement)
layer_surf_disp = np.zeros_like(substrate_surf_disp)

# Compute displacements
model.operators["mindlin"](σ, model.displacement)  # Eigenstress
model.operators["boussinesq"](model.traction,
                              substrate_surf_disp)  # Surface tractions
model.displacement[:] += substrate_surf_disp

layer_model.operators["boussinesq"](model.traction, layer_surf_disp)


# Computing with Li & Popov (2020)
u = normal_layered_disp(model, layer_model, model.traction[..., direction])


# Displacement normalization
def unorm(m):
    return tau * L / (np.pi * m.E_star)


# Plot displacement along the y direction
yline = np.s_[0, 0, :, direction]
ycoord = y

# Substrate displacement in homogeneous case
plt.axhline(1, color='k', ls='--')

# Layer ux in homogeneous case
plt.axhline(unorm(layer_model) / unorm(model), color='red', ls='--')

# Plotting homogeneous displacements
plt.plot(ycoord,
         substrate_surf_disp[yline] / unorm(model),
         'k--',
         label='Homogeneous substrate', lw=3)
plt.plot(ycoord,
         layer_surf_disp[yline] / unorm(model),
         'r--',
         label='Homogeneous layer', lw=3)

# Plotting analytical solution
plt.plot(
    ycoord,
    u[0] / unorm(model),
    lw=3,
    label='Li & Popov (2020)')

# Plotting composite displacement
plt.plot(
    ycoord,
    model.displacement[yline] / unorm(model),
    label=f'$L_z/L$ = {Lz/L:.1f}, $E_S/E_L$ = {model.E / layer_model.E:.0f} (Eshelby)')

plt.legend(fontsize='x-small')

plt.xlabel('$x$ [$L$]')
plt.ylabel('$u_x$ [$\\pi E_S^* / (\\tau L)$]')

plt.gcf().tight_layout()
plt.savefig('displacement.pdf' if len(sys.argv) <= 1 else sys.argv[1],
            bbox_inches='tight')
plt.show()

# Write Numpy file with displacements
NumpyDumper("layer", "displacement", mkdir=False) << model
