{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9154e89d-9fd1-4829-a887-68a024c46afc",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "import tamaas as tm\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from copy import deepcopy\n",
    "from scipy.sparse.linalg import LinearOperator, aslinearoperator, bicgstab\n",
    "from tamaas.dumpers import NumpyDumper"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c22db7d-41eb-4a8a-8107-2f6bef721698",
   "metadata": {},
   "outputs": [],
   "source": [
    "def normal_layered_green(model, layer_model):\n",
    "    \"\"\"Plane-strain Green's function from Li & Popov (2020)\"\"\"\n",
    "    shape = model.boundary_shape\n",
    "    L = model.system_size[1:]\n",
    "    h = layer_model.system_size[0]\n",
    "    q = np.fft.fftfreq(shape[1], d=L[1]/shape[1]) * 2 * np.pi\n",
    "    q = np.abs(q)\n",
    "\n",
    "    E2, nu2 = model.E, model.nu\n",
    "    E1, nu1 = layer_model.E, layer_model.nu\n",
    "\n",
    "    A = (\n",
    "        (E2 * (3 - 4 * nu1) * (1 + nu1) - E1 * (3 - 4 * nu2) * (1 + nu2))\n",
    "        * (E1 * (1 + nu2) - E2 * (1 + nu1))\n",
    "    )\n",
    "\n",
    "    B = 4 * (\n",
    "        (E2 * (1 + nu1) + E1 * (3 - 4 * nu2) * (1 + nu2))\n",
    "        * (E1 * (1 + nu2) - E2 * (1 + nu1))\n",
    "    )\n",
    "\n",
    "    C = (\n",
    "        E1**2 * (4 * nu2 - 3) * (1 + nu2)**2\n",
    "        - 2 * E1 * E2 * (1 + nu1) * (2 * nu1 - 1) * (nu2 + 1) * (2 * nu2 - 1)\n",
    "        + E2**2 * (8 * nu1**2 - 12 * nu1 + 5) * (1 + nu1)**2\n",
    "    )\n",
    "\n",
    "    D = (\n",
    "        (E2 * (1 + nu1) + E1 * (3 - 4 * nu2) * (1 + nu2))\n",
    "        * (E2 * (3 - 4 * nu1) * (1 + nu1) + E1 * (1 + nu2))\n",
    "    )\n",
    "\n",
    "    with np.errstate(divide='ignore'):\n",
    "        G = 2 / layer_model.E_star * (\n",
    "            (A * np.exp(-4 * q * h) + B * q * h * np.exp(- 2 * q * h) + D)\n",
    "            / (q * (-A * np.exp(-4 * q * h) - B * q**2 * h**2 * np.exp(-2 * q * h)\n",
    "                    + 2 * C * np.exp(-2 * q * h) + D))\n",
    "        )\n",
    "\n",
    "    G[0] = 0\n",
    "\n",
    "    return G\n",
    "\n",
    "\n",
    "def normal_layered_disp(model, layer_model, traction):\n",
    "    pfft = np.fft.fft2(traction)\n",
    "    G = normal_layered_green(model, layer_model)\n",
    "    return np.fft.ifft2(G[np.newaxis, :] * pfft).real"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7946ea7-cdfe-4e09-99a7-52752740d854",
   "metadata": {},
   "source": [
    "## Problem variables\n",
    "The number of points in x direction is set to 2 because we look at\n",
    "a plane-strain problem to compare to the analytical solution,\n",
    "but the method is fully 3D."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1516ffbe-b295-4a0d-a7d4-b14242597fa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "Nx = 2\n",
    "Ny = 3**3\n",
    "Nz = 256\n",
    "L = 1\n",
    "Lz = 1e-1  # thickness of layer\n",
    "tau = 1\n",
    "R = L / 24  # radius of shear circle\n",
    "direction = 2  # 0 = x, 1 = y, 2 = z, direction of applied load"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c179f023-56e6-409b-8747-5676df823477",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Model for the homogeneous of the half-space\n",
    "model = tm.Model(tm.model_type.volume_2d, [Lz, L, L], [Nz, Nx, Ny])\n",
    "\n",
    "# Same Poisson ratio\n",
    "model.nu = 0.0\n",
    "\n",
    "# Set Young's modulus for layer\n",
    "layer_model = deepcopy(model)\n",
    "layer_model.E *= 0.5\n",
    "\n",
    "# Register the volume integral operators (Mindlin)\n",
    "tm.ModelFactory.registerVolumeOperators(model)\n",
    "tm.ModelFactory.registerVolumeOperators(layer_model)\n",
    "\n",
    "# Need to set integration method to cutoff:\n",
    "# It is a bit slower but it does not care about\n",
    "# the layer thickness\n",
    "for op in [\"mindlin\", \"mindlin_gradient\"]:\n",
    "    for m in [model, layer_model]:\n",
    "        tm.ModelFactory.setIntegrationMethod(m.operators[op],\n",
    "                                             tm.integration_method.cutoff,\n",
    "                                             1e-10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b6910f8-e9df-4b01-b486-64885dd51b30",
   "metadata": {},
   "source": [
    "## Surface stress distribution\n",
    "We apply a surface pressure in the form of $\\tau(y) = \\tau \\sin\\left(2\\pi\\frac{y}{L}\\right)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a79048f-b8e0-4b98-802b-d0f08405e775",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.linspace(0, L, Nx, endpoint=False)\n",
    "y = np.linspace(0, L, Ny, endpoint=False)\n",
    "xx, yy = np.meshgrid(x, y, indexing='ij', sparse=True)\n",
    "\n",
    "# Applied load in the specified direction\n",
    "model.traction[..., direction] = tau * np.sin(2 * np.pi * yy / L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d43004f8-54b7-4e80-b55b-3b949045e77a",
   "metadata": {},
   "source": [
    "Construct the RHS of the linear equation we want to solve:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4ce7c8ab-5d32-486e-981e-3855f8c9a8e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "model['rhs'] = np.zeros(model.shape + [6])  # 6 strain components in Voigt\n",
    "model.operators['boussinesq_gradient'](model.traction, model['rhs'])\n",
    "\n",
    "# Flatten\n",
    "rhs = model['rhs'].reshape(-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e269ef4b-7527-4130-8bc9-dbfbbbda724a",
   "metadata": {},
   "source": [
    "## Solving for the total deformation tensor\n",
    "\n",
    "Define the linear operators in the equation to solve:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57260740-9737-4594-894b-b2e6b957eea8",
   "metadata": {},
   "outputs": [],
   "source": [
    "C1 = aslinearoperator(layer_model.operators['hooke'])\n",
    "C2 = aslinearoperator(model.operators['hooke'])\n",
    "ΔC = C2 - C1\n",
    "grad_N2 = aslinearoperator(model.operators['mindlin_gradient'])\n",
    "\n",
    "# Define the linear operator we want to invert\n",
    "A = LinearOperator(shape=[rhs.size, rhs.size],\n",
    "                   matvec=lambda x: x - grad_N2 * ΔC * x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9fe141a-edee-444c-b380-cdd3fecb4946",
   "metadata": {},
   "source": [
    "Solve for the total deformation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aae6b693-dad5-4052-8bf7-abcd5edb0b60",
   "metadata": {},
   "outputs": [],
   "source": [
    "ɛ, _ = bicgstab(A, rhs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0a711cd-3d07-4e18-ae04-2cdd975ec74f",
   "metadata": {},
   "source": [
    "## Reconstrucing the inhomogeneous displacement\n",
    "\n",
    "Compute the equivalent eigenstresses, which we will use to compute displacements due to inhomogeneity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa57623f-6509-4b16-bcda-2f26f8099710",
   "metadata": {},
   "outputs": [],
   "source": [
    "σ = ΔC * ɛ\n",
    "σ = σ.reshape(model.shape + [6])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfac6f38-2381-4e5b-9e3f-9d9fbb9f6003",
   "metadata": {},
   "source": [
    "Define arrays for displacements due to only surface tractions for each model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02f02338-c1b7-408a-b882-2664298e32ab",
   "metadata": {},
   "outputs": [],
   "source": [
    "substrate_surf_disp = np.zeros_like(model.displacement)\n",
    "layer_surf_disp = np.zeros_like(substrate_surf_disp)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9d4897e-6d43-45cb-9d9f-de533a21f403",
   "metadata": {},
   "source": [
    "Compute full displacements, store in `model.displacement`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99aec2b5-d3c8-44b5-b961-f31e7f75d123",
   "metadata": {},
   "outputs": [],
   "source": [
    "model.operators[\"mindlin\"](σ, model.displacement)  # Eigenstress\n",
    "model.operators[\"boussinesq\"](model.traction,\n",
    "                              substrate_surf_disp)  # Surface tractions\n",
    "model.displacement[:] += substrate_surf_disp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "655850e2-f011-450d-bd78-51c226cc0f57",
   "metadata": {},
   "source": [
    "Compute homogeneous displacement for the layer (for reference):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a57c7875-7ae4-47a2-b07f-9bc60dcaad62",
   "metadata": {},
   "outputs": [],
   "source": [
    "layer_model.operators[\"boussinesq\"](model.traction, layer_surf_disp)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "609009a2-fbae-4309-b316-da9b42069d07",
   "metadata": {},
   "source": [
    "Compute inhomogeneous displacements from Li & Popov (2020):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95050b24-e1d2-42f3-a8ca-58564a53648b",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = normal_layered_disp(model, layer_model, model.traction[..., direction])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8ca34e6-65bb-40cb-a74f-8b86d0b5747b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def unorm(m):\n",
    "    return tau * L / (np.pi * m.E_star)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56fbf3b7-a874-441d-842b-fef268b07536",
   "metadata": {},
   "source": [
    "## Plotting the displacements\n",
    "\n",
    "In this figure, we plot:\n",
    "\n",
    "- The inhomogeneous displacement field computed with the Eshelby method\n",
    "- The inhomogeneous displacement field computed with the direct Green's function from Li & Popov (2020)\n",
    "- The homogeneous displacement for the substrate\n",
    "- The homogeneous displacement for the layer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "784e715d-7e03-4d94-8d7a-d3c448a10a79",
   "metadata": {},
   "outputs": [],
   "source": [
    "yline = np.s_[0, 0, :, direction]\n",
    "ycoord = y\n",
    "\n",
    "# Substrate displacement in homogeneous case\n",
    "plt.axhline(1, color='k', ls='--')\n",
    "\n",
    "# Layer ux in homogeneous case\n",
    "plt.axhline(unorm(layer_model) / unorm(model), color='red', ls='--')\n",
    "\n",
    "# Plotting homogeneous displacements\n",
    "plt.plot(ycoord,\n",
    "         substrate_surf_disp[yline] / unorm(model),\n",
    "         'k--',\n",
    "         label='Homogeneous substrate', lw=3)\n",
    "plt.plot(ycoord,\n",
    "         layer_surf_disp[yline] / unorm(model),\n",
    "         'r--',\n",
    "         label='Homogeneous layer', lw=3)\n",
    "\n",
    "# Plotting analytical solution\n",
    "plt.plot(\n",
    "    ycoord,\n",
    "    u[0] / unorm(model),\n",
    "    lw=3,\n",
    "    label='Li & Popov (2020)')\n",
    "\n",
    "# Plotting composite displacement\n",
    "plt.plot(\n",
    "    ycoord,\n",
    "    model.displacement[yline] / unorm(model),\n",
    "    label=f'$L_z/L$ = {Lz/L:.1f}, $E_S/E_L$ = {model.E / layer_model.E:.0f} (Eshelby)')\n",
    "\n",
    "plt.legend(fontsize='x-small')\n",
    "\n",
    "plt.xlabel('$x$ [$L$]')\n",
    "plt.ylabel('$u_x$ [$\\\\pi E_S^* / (\\\\tau L)$]')\n",
    "\n",
    "plt.gcf().tight_layout()\n",
    "plt.savefig('displacement.pdf', bbox_inches='tight', transparent=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ea50457-871c-4faa-8c77-4f6ab00b87c2",
   "metadata": {},
   "source": [
    "## Writing data to Numpy file\n",
    "\n",
    "We write the displacement field into a Numpy file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a98c21ca-733a-48da-81e0-ddf2fae60434",
   "metadata": {},
   "outputs": [],
   "source": [
    "NumpyDumper(\"layer\", \"displacement\", mkdir=False) << model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22584c2e-8e64-4982-b61b-09af6fa2788a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
